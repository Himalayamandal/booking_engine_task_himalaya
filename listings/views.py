from rest_framework.views import APIView
from django.db.models import Q
from rest_framework.response import Response
from rest_framework import status
from .serializers import RoomBookingDetailsSerializer
from .models import BookingInfo, HotelRoomBookModel


class RoomsAvailableView(APIView):
    
    def get(self, request):
        check_in = request.query_params.get('check_in')
        check_out = request.query_params.get('check_out')
        max_price = request.query_params.get('max_price')
        booked_rooms = HotelRoomBookModel.objects.filter(
                Q(
                    check_in__lte=check_in,
                    check_out__gte=check_in
                ) | Q(
                    check_in__lte=check_out,
                    check_out__gte=check_out
                ) | Q(
                    check_in__gte=check_in,
                    check_out__lte=check_out
                )
            ).values_list('booking_info_id', flat=True)
        available_rooms = BookingInfo.objects.filter(price__lt=max_price).order_by('price').exclude(id__in=booked_rooms)

        if not available_rooms:
            return Response(
                    {
                        "error_msg": "Rooms are not available!"
                    },
                    status=status.HTTP_200_OK
                )
        available_rooms_listing = RoomBookingDetailsSerializer(available_rooms, many=True).data
        
        return Response(
                {
                    "items": available_rooms_listing
                },
                status=status.HTTP_200_OK
            )